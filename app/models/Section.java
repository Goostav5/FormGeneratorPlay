package models;

import com.avaje.ebean.Model;
import org.springframework.context.annotation.Lazy;
import play.data.validation.Constraints;
import play.data.validation.ValidationError;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alek on 2015-12-02.
 */
@Entity
@Table(name = "fm_sections")
public class Section extends Model {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column(name = "fm_sec_id")
    public Long id;

    @Constraints.MaxLength(60)
    @Constraints.Required
    @Column(name = "fm_sec_name")
    public String name;

    @ManyToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
    @JoinTable(
            name = "fm_assignment",
            joinColumns = {
                    @JoinColumn(name = "fm_sec_id", referencedColumnName = "fm_sec_id")},
            inverseJoinColumns = {
                    @JoinColumn(name = "fm_usr_id", referencedColumnName = "fm_usr_id")}
    )
    public List<User> users;

    @ManyToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
    @JoinTable(
            name = "fm_access",
            joinColumns = {
                    @JoinColumn(name = "fm_sec_id", referencedColumnName = "fm_sec_id")},
            inverseJoinColumns = {
                    @JoinColumn(name = "fm_frm_id", referencedColumnName = "fm_frm_id")}
    )
    public List<Formularz> forms;

    public List<ValidationError> validate() {
        List<ValidationError> errors = new ArrayList<ValidationError>();

        //sprawdzenie czy podana nazwa grupy jest zajęta

            Finder<Long, Section> finder = new Finder<Long, Section>(Section.class);
            List<Section> nameTakenBy = finder.where().ilike("fm_sec_name", name).findList();
            if (!nameTakenBy.isEmpty()) {
                errors.add(new ValidationError("name", "Podana nazwa sekcji jest zajęta!"));
            }


        return errors.isEmpty() ? null : errors;
    }
}
