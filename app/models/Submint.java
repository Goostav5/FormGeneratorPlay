package models;

import javax.persistence.*;
import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.Formula;

import java.util.List;

@Entity
@Table(name = "fm_submits")
public class Submint extends Model {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column(name = "fm_fms_id")
    public Long id;

    @Column(name = "fm_fms_completed")
    public Boolean completed;

    @ManyToOne
    @JoinColumn(name="fm_usr_id", referencedColumnName = "fm_usr_id")
    public User userAnswer;

    @ManyToOne
    @JoinColumn(name = "fm_frm_id", referencedColumnName = "fm_frm_id")
    public Formularz formularz;

    @ManyToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
    @JoinTable(
            name = "fm_submints_answers",
            joinColumns = {
                    @JoinColumn(name = "fm_fms_id", referencedColumnName = "fm_fms_id")},
            inverseJoinColumns = {
                    @JoinColumn(name = "fm_ans_id", referencedColumnName = "fm_ans_id")}
    )
    public List<Answer> answers;

    //metoda sprawdzajaca czy submint zawiera juz odpowiedzi na wszystkie pytania
    public Boolean isCompleted(){
        Boolean found;
        for(Question q: this.formularz.questions){
            found = false;
            for(Answer a: this.answers){
                if(a.question.id == q.id){
                    //znaleziono odpowiedz, nie szukaj dalej
                    found = true;
                    break;
                }
            }

            //nie znaleziono odpowiedzi, submint nie kompletny
            if(!found){
                return false;
            }
        }

        //znaleziono odpowiedzi na wszystkie pytania
        this.completed = true;
        this.update();
        return true;
    }
}
