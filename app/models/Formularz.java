package models;

import java.util.*;
import javax.persistence.*;

import com.avaje.ebean.Model;
import play.data.format.*;
import play.data.validation.*;

@Entity
@Table(name = "fm_forms")
public class Formularz extends Model {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column(name = "fm_frm_id")
    public Long id;

    @Constraints.Required
    @Constraints.MaxLength(50)
    @Column(name = "fm_frm_name")
    public String name;

    @Formats.DateTime(pattern="dd/MM/yyyy")
    @Column(name = "fm_frm_deadline")
    public Date deadline;

    @Column(name = "fm_frm_accepted")
    public Boolean accepted;

    @ManyToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
    @JoinTable(
            name = "fm_access",
            joinColumns = {
                    @JoinColumn(name = "fm_frm_id", referencedColumnName = "fm_frm_id")},
            inverseJoinColumns = {
                    @JoinColumn(name = "fm_sec_id", referencedColumnName = "fm_sec_id")}
    )
    public List<Section> sections;

    @OneToMany(mappedBy = "formularz", cascade = CascadeType.ALL)
    public List<Question> questions;

    @OneToMany(mappedBy = "formularz", cascade = CascadeType.ALL)
    public List<Submint> submints;
}