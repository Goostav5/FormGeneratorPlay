package models;

import java.util.*;
import javax.persistence.*;
import com.avaje.ebean.Model;
import play.data.format.*;
import play.data.validation.*;

@Entity
@Table(name = "fm_answers")
public class Answer extends Model {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column(name = "fm_ans_id")
    public Long id;

    @Constraints.Required
    @Constraints.MaxLength(500)
    @Column(name = "fm_ans_answer")
    public String answer;

    @ManyToOne
    @JoinColumn(name="fm_que_id", referencedColumnName = "fm_que_id")
    public Question question;

    @ManyToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
    @JoinTable(
            name = "fm_submints_answers",
            joinColumns = {
                    @JoinColumn(name = "fm_ans_id", referencedColumnName = "fm_ans_id")},
            inverseJoinColumns = {
                    @JoinColumn(name = "fm_fms_id", referencedColumnName = "fm_fms_id")}
    )
    public List<Submint> submints;

}