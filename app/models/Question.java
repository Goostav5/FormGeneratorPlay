package models;

import java.util.*;
import javax.persistence.*;

import com.avaje.ebean.Model;
import play.data.format.*;
import play.data.validation.*;

@Entity
@Table(name = "fm_questions")
public class Question extends Model {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column(name = "fm_que_id")
    public Long id;

    @Constraints.Required
    @Constraints.MaxLength(200)
    @Column(name = "fm_que_question")
    public String question;

    @Constraints.Required
    @Column(name = "fm_que_type")
    public Short questionType;

    @ManyToOne
    @JoinColumn(name="fm_frm_id", referencedColumnName = "fm_frm_id")
    public Formularz formularz;


    @OneToMany(mappedBy = "question", cascade = CascadeType.ALL)
    public List<Answer> answers;

}