package models;

import com.avaje.ebean.Model;
import org.springframework.context.annotation.Lazy;
import play.data.validation.Constraints;
import play.data.validation.ValidationError;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
	* Created by Marcin on 2015-11-26.
 */

@Entity
@Table(name = "fm_users")
public class User extends Model {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column(name = "fm_usr_id")
    public Long id;

    @Constraints.MaxLength(50)
    @Column(name = "fm_usr_name")
    public String name;

    @Constraints.MaxLength(50)
    @Column(name = "fm_usr_surname")
    public String surname;

    @Constraints.MaxLength(50)
    @Column(name = "fm_usr_email")
    public String email;

    @Constraints.Required
    @Constraints.MaxLength(20)
    @Column(unique = true, name = "fm_usr_login")
    public String login;

    @Constraints.Required
    @Column(name = "fm_usr_password")
    public String password;

    @Transient
    @Constraints.Required
    public String repeatPassword;

    @Column(name = "fm_usr_admin")
    public Boolean admin;

    @Transient
    //flaga która wskazuje na to czy użytkownik jest tworzony czy edytowany
    public Boolean update = false;

    @ManyToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
    @JoinTable(
            name = "fm_assignment",
            joinColumns = {
                    @JoinColumn(name = "fm_usr_id", referencedColumnName = "fm_usr_id")},
            inverseJoinColumns = {
                    @JoinColumn(name = "fm_sec_id", referencedColumnName = "fm_sec_id")}
    )
    public List<Section> sections;

    @OneToMany(mappedBy = "userAnswer", cascade = CascadeType.ALL)
    public List<Submint> submints;

    //metoda implementująca dodatkową walidację
    public List<ValidationError> validate() {
        List<ValidationError> errors = new ArrayList<ValidationError>();

        //sprawdzenie czy podane hasła są identyczne
        if (!repeatPassword.equals(password)) {
            errors.add(new ValidationError("repeatPassword", "Podane hasła nie są identyczne"));
        }

        //sprawdzenie czy podany login już istnieje, tylko jesli tworzony jest nowy użytkownik
        if(!update) {
            Finder<Long, User> finder = new Finder<Long, User>(User.class);
            List<User> loginTakenBy = finder.where().ilike("fm_usr_login", login).findList();
            if (!loginTakenBy.isEmpty()) {
                errors.add(new ValidationError("login", "Podany login już istnieje"));
            }
        }

        return errors.isEmpty() ? null : errors;
    }

}
