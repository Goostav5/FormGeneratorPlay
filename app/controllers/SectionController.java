package controllers;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Model;
import com.avaje.ebean.text.PathProperties;
import com.fasterxml.jackson.databind.JsonNode;
import models.Formularz;
import models.Section;
import models.User;
import play.Logger;
import play.data.DynamicForm;
import play.libs.Json;
import play.data.Form;
import play.mvc.*;

import views.html.*;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections4.ListUtils;

/**
 * Created by Alek on 2015-12-02.
 */
//dostep do kontrolera tylko dla admina
@Security.Authenticated(AdminAuth.class)
public class SectionController extends Controller {

    //usuwanie formularza z sekcji
    public Result deleteForm(Long id, Long fmId){
        Formularz formularz = Ebean.find(Formularz.class, fmId);
        Section section = Ebean.find(Section.class, id);
        section.forms.remove(formularz);
        section.update();
        flash("section-forms-alert", "Wybrany formularz został usunięty z sekcji");
        return redirect("/sections/view/"+id.toString()+"?tab=forms");
    }

    //dodawanie formularzy do sekcji
    public Result addForm(Long id, Long fmId){
        Section section = Ebean.find(Section.class, id);
        String httpMethod = request().method();

        //jesli POST, dodaj wybrany formularz
        if(httpMethod.equals("POST")){
            Formularz form = Ebean.find(Formularz.class, fmId);

            //sprawdz czy wybrany formularz zostal zaakceptowany
            if(!form.accepted){
                return status(403, "Formularz nie zostal jeszcze zaakceptowany");
            }

            section.forms.add(form);
            section.update();
            flash("section-forms-alert", "Wybrany formularz został dodany do sekcji");
            return redirect("/sections/view/"+id.toString()+"?tab=forms");
        }

        //jesli GET, wyświetl formularze które nie należą do sekcji i sa zaakceptowane
        List<Formularz> allForms = Ebean.find(Formularz.class).where().eq("accepted", true).findList();
        //utworzenie listy formularzy które nie należą do sekcji
        List<Formularz> formsNotInSection = ListUtils.subtract(allForms, section.forms);
        return ok(sectionaddform.render(section, formsNotInSection));
    }

    //usuwanie członka sekcji
    public Result deleteMember(Long id, Long usId){
        User user = Ebean.find(User.class, usId);
        Section section = Ebean.find(Section.class, id);
        section.users.remove(user);
        section.update();
        flash("section-members-alert", "Wybrany użytkownik został usunięty z listy członków sekcji");
        return redirect("/sections/view/"+id.toString()+"?tab=members");
    }

    //dodawanie użytkowników do sekcji
    public Result addMember(Long id, Long usId){
        Section section = Ebean.find(Section.class, id);
        String httpMethod = request().method();

        //jesli POST, dodaj wybranego uzytkownika
        if(httpMethod.equals("POST")){
            User user = Ebean.find(User.class, usId);
            section.users.add(user);
            section.update();
            flash("section-members-alert", "Wybrany użytkownik został dodany do listy członków sekcji");
            return redirect("/sections/view/"+id.toString()+"?tab=members");
        }

        //jesli GET, wyświetl uzytkowników którzy nie są członkami sekcji
        List<User> allUsers = Ebean.find(User.class).where().findList();
        //utworzenie listy użytkowników którzy nie są członkami sekcji
        List<User> notMembers = ListUtils.subtract(allUsers, section.users);
        return ok(sectionaddmember.render(section, notMembers));
    }

    //wyswietlanie szczegółów sekcji
    public Result view(Long id, String tab){
        Section section = Ebean.find(Section.class, id);
        return ok(sectionview.render(section, tab));
    }

    //dodawanie sekcji
    public Result create(){
        Form<Section> sectionForm = Form.form(Section.class);
        String httpMethod = request().method();

        //jeśli metoda POST, odebrano formularz
        if(httpMethod.equals("POST")){
            //odczytanie danych z odebranego formularza
            sectionForm = sectionForm.bindFromRequest();

            //walidacja danych
            if(!sectionForm.hasErrors()){
                //zapisanie do bazy
                Section section = sectionForm.get();
                section.save();
                return redirect(routes.SectionController.indexSections());
            }

        }

        //wyświetlenie formularza
        return ok(sectionform.render(sectionForm, false, "Dodaj sekcję"));
    }

    //edycja sekcji
    public Result update(Long id){
        Section section = new Model.Finder<Long, Section>(Section.class).byId(id);
        Form<Section> sectionForm = Form.form(Section.class);
        String httpMethod = request().method();

        //jeśli metoda POST, odebrano formularz
        if(httpMethod.equals("POST")){
            //odczytanie danych z odebranego formularza
            sectionForm = sectionForm.bindFromRequest();

            //walidacja danych
            if(!sectionForm.hasErrors()){
                //zapisanie do bazy
                section = sectionForm.get();
                section.id = id;
                section.update();
                return redirect(routes.SectionController.indexSections());
            }

        }else{
            sectionForm = sectionForm.fill(section);
        }

        //wyświetlenie formularza
        return ok(sectionform.render(sectionForm, false, "Edytuj sekcję"));
    }

    //usuwanie sekcji
    public Result delete(Long id){
        //flash który zostanie wyświetlony po przekierowaniu na stronie indexsection
        flash("sections-alert", "Sekcja została usunięta");
        //usunięcie sekcji
        new Model.Finder<Long, Section>(Section.class).ref(id).delete();
        return redirect(routes.SectionController.indexSections());
    }

    //wyświetlenie strony do zarządzania sekcjami
    public Result indexSections(){
        PathProperties pathProperties = PathProperties.parse("(id, name)");
        List<Section> allSections = Ebean.find(Section.class).apply(pathProperties).orderBy("id").findList();
        return ok(indexsections.render(allSections));
    }

    /*public Result getUsers(Long id){
        PathProperties pathProperties = PathProperties.parse("(name)");
        Section section = Ebean.find(Section.class, id);
        return ok(Ebean.json().toJson(section.users, pathProperties));
    }

    public Result addUser(Long secId, Long usId){
        Section section = Ebean.find(Section.class, secId);
        if(section !=null) {
            if (section.users != null) {
                for (User us : section.users) {
                    if (us.id == usId) return ok(Ebean.json().toJson("User Exisit"));
                }
            }
            User user = Ebean.find(User.class, usId);
            if (user != null) {
                section.users.add(user);
                section.update();
                return redirect("/sections/view/" + section.id.toString());
            }
            return ok(Ebean.json().toJson("No user found"));
        }
        else {
            return ok(Ebean.json().toJson("Section no exist"));
        }
    }

    public Result removeUser(Long secId, Long usId) {
        Section section = Ebean.find(Section.class, secId);
        if (section != null) {
            if (section.users != null) {
                for (User us : section.users) {
                    if (us.id == usId) {
                        section.users.remove(us);
                        section.update();
                        return redirect("/sections/view/" + section.id.toString());
                    }
                }
            }
            return ok(Ebean.json().toJson("User don't belong to section"));
        } else {
            return ok(Ebean.json().toJson("Section no exist"));
        }
    }

    public Result getForms(Long id){
        PathProperties pathProperties = PathProperties.parse("(name)");
        Section section = Ebean.find(Section.class, id);
        return ok(Ebean.json().toJson(section.forms, pathProperties));
    }

    public Result addForm(Long secId, Long fmId){
        Section section = Ebean.find(Section.class, secId);
        if(section !=null) {
            if (section.forms != null) {
                for (Formularz fm : section.forms) {
                    if (fm.id == fmId) return ok(Ebean.json().toJson("Form Exisit"));
                }
            }
            Formularz form = Ebean.find(Formularz.class, fmId);
            if (form != null) {
                section.forms.add(form);
                section.update();
                return ok(Ebean.json().toJson("Form Added to section"));
            }
            return ok(Ebean.json().toJson("No valid form found"));
        }
        else {
            return ok(Ebean.json().toJson("Section no exist"));
        }
    }

    public Result removeForm(Long secId, Long fmId) {
        Section section = Ebean.find(Section.class, secId);
        if (section != null) {
            if (section.forms != null) {
                for (Formularz fm : section.forms) {
                    if (fm.id == fmId) {
                        section.forms.remove(fm);
                        section.update();
                        return ok(Ebean.json().toJson("Form Removed"));
                    }
                }
            }
            return ok(Ebean.json().toJson("Form don't accesed to section"));
        } else {
            return ok(Ebean.json().toJson("Section no exist"));
        }
    }*/

}
