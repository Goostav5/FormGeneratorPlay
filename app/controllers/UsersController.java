package controllers;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Model;
import com.avaje.ebean.text.PathProperties;
import com.fasterxml.jackson.databind.JsonNode;
import models.Formularz;
import models.User;
import models.*;
import play.Logger;
import play.data.DynamicForm;
import play.libs.Json;
import play.data.Form;
import play.mvc.*;

import views.html.*;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class UsersController extends Controller {

    //wyświetlenie i obsługa formularza dodającego użytkownika
    //@Security.Authenticated(AdminAuth.class)
    public Result addUser() {
        Form<User> userForm = Form.form(User.class);
        String httpMethod = request().method();

        //jeśli metoda POST, odebrano formularz
        if(httpMethod.equals("POST")){
            //odczytanie danych z odebranego formularza
            userForm = userForm.bindFromRequest();

            //walidacja danych
            if(!userForm.hasErrors()){
                //zapisanie do bazy
                User user = userForm.get();
                user.save();
                return redirect(routes.UsersController.indexUsers());
            }

        }

        //wyświetlenie formularza
        return ok(userform.render(userForm, false, "Dodaj użytkownika"));
    }

    //pobranie wszystkich userów i zwrócenie jako JSON
    @Security.Authenticated(AdminAuth.class)
    public Result getUsers(){
        PathProperties pathProperties = PathProperties.parse("(id, name, surname, email, login, admin)");
        List<User> allUsers = Ebean.find(User.class).apply(pathProperties).orderBy("id").findList();

        //zubodwanie jsona, zmienna pathProperties dodana po to żeby, toJson() nie pobrał automatycznie pozostałych pól
        return ok(Ebean.json().toJson(allUsers, pathProperties));
    }

    //wyświetlenie strony do zarządzania userami
    @Security.Authenticated(AdminAuth.class)
    public Result indexUsers(){
        return ok(indexusers.render());
    }

    //wyświetlenie informacji o userze
    public Result view(Long id){
        PathProperties pathProperties = PathProperties.parse("(id, name, surname, email, login, admin)");
        User user = Ebean.find(User.class).apply(pathProperties).setId(id).findUnique();
        return ok(viewuser.render(user));
    }

    //usuwanie usera
    @Security.Authenticated(AdminAuth.class)
    public Result deleteUser(Long id){
        //flash który zostanie wyświetlony po przekierowaniu na stronie indexusers
        flash("users-alert", "Użytkownik został usunięty");
        //usunięcie użytkownika
        new Model.Finder<Long, User>(User.class).ref(id).delete();
        return redirect(routes.UsersController.indexUsers());
    }

    //updateowanie usera
    @Security.Authenticated(AdminAuth.class)
    public Result updateUser(Long id){
        String httpMethod = request().method();
        User user = new Model.Finder<Long, User>(User.class).byId(id);
        Form<User> form = Form.form(User.class);

        if(httpMethod.equals("POST")){
            Map<String, String[]> requestData = request().body().asFormUrlEncoded();
            Map<String, String> formFields = new HashMap();

            //ręczne przepisanie pól z formularza, żeby ustawić hasło jeśli nie jest wpisane i zabezpieczyć przed zmianą loginu
            formFields.put("name", requestData.get("name")[0]);
            formFields.put("surname", requestData.get("surname")[0]);
            formFields.put("email", requestData.get("email")[0]);
            formFields.put("login", user.login);
            if(requestData.get("password")[0].equals("") && requestData.get("repeatPassword")[0].equals("")) {
                formFields.put("password", user.password);
                formFields.put("repeatPassword", user.password);
            }else{
                formFields.put("password", requestData.get("password")[0]);
                formFields.put("repeatPassword", requestData.get("repeatPassword")[0]);
            }
            if(requestData.get("admin") != null){
                formFields.put("admin", "true");
            }
            else{
                formFields.put("admin", "false");
            }
            formFields.put("update", "true");
            form = form.bind(formFields);

            //walidacja danych
            if(!form.hasErrors()){
                //zapisanie do bazy
                user = form.get();
                user.id = id;
                user.update();
                flash("users-alert", "Dane użytkownika zostały zmienione");
                return redirect(routes.UsersController.view(id));
            }
        }else {
            form = form.fill(user);
        }

        return ok(userform.render(form, true, "Edytuj użytkownika"));
    }

}
