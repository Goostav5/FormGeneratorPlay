package controllers;

import com.avaje.ebean.Ebean;
import models.User;
import play.data.Form;
import play.mvc.*;
import views.html.*;

/**
 * Created by Marcin on 2015-12-08.
 */
public class SiteController extends Controller{

    //klasa reprezentująca formularz logowania
    public static class Login {
        public String username;
        public String password;
        public User user;
        public String validate() {
            user = Ebean.find(User.class).where().ilike("login", username).ilike("password", password).findUnique();
            if (user == null) {
                //nie wiem czemu ale standardowe błędy się nie wyświetlają w formularzu więc wyświetlam flasha
                flash("login-alert", "Zły login lub hasło");
                //standardowe zgłoszenie błędu, nie wyświetla się, ale trzeba dodać żeby działało
                return "Invalid user or password";
            }
            return null;
        }
    }

    //strona główna, logowanie
    public Result index() {
        //jeśli użytkownik zalogowany, przekirowanie na stronę admina lub użytkownika
        if(session("admin") != null) {
            if (session("admin").equals("true")) {
                return redirect(routes.SiteController.adminMain());
            }
            if (session("admin").equals("false")) {
                return redirect(routes.SiteController.userMain());
            }
        }

        String httpMethod = request().method();
        Form<Login> loginForm = Form.form(Login.class);

        if(httpMethod.equals("POST")){
            loginForm = loginForm.bindFromRequest();

            if (loginForm.hasErrors()) {
                return badRequest(loginpage.render(loginForm));
            } else {
                //zapisanie danych o zalogowanym uzytkowniku w sesji
                session().clear();
                session("login", loginForm.get().user.login);
                session("userid", loginForm.get().user.id.toString());
                if(loginForm.get().user.admin != null){
                    session("admin", "true");
                    return redirect(routes.SiteController.adminMain());
                }else{
                    session("admin", "false");
                    return redirect(routes.SiteController.userForms());
                }

            }
        }

        return ok(loginpage.render(loginForm));
    }

    //strona głowna dla admina
    //dostep tylko dla admina
    @Security.Authenticated(AdminAuth.class)
    public Result adminMain(){
        return ok(adminmain.render());
    }

    //strona głowna dla zwykłego użytkownika
    @Security.Authenticated(UserAuth.class)
    public Result userMain(){return ok(usermain.render());}

    //strona wyswietlajaca formularze ktore moze wypelnic uzytkownik
    @Security.Authenticated(UserAuth.class)
    public Result userForms(){
        User user = Ebean.find(User.class).setId(Integer.parseInt(session("userid"))).findUnique();
        return ok(userforms.render(user));
    }

    //wylogowanie
    public Result logout(){
        session().clear();
        return redirect(routes.SiteController.index());
    }
}
