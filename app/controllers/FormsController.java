package controllers;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Model;
import com.avaje.ebean.text.PathProperties;
import models.Answer;
import models.Formularz;
import models.Question;
import play.data.Form;
import play.mvc.*;
import views.html.*;

import java.util.Collections;
import java.util.List;
import java.util.Map;

//dostep do kontrolera tylko dla admina
@Security.Authenticated(AdminAuth.class)
public class FormsController extends Controller {

    public Result accept(Long id){
        Formularz formularz = Ebean.find(Formularz.class, id);
        formularz.accepted = true;
        formularz.update();
        return redirect(routes.FormsController.view(id, "questions"));
    }

    //usuwanie pytania
    public Result deleteQuestion(Long qsId){
        Question question  = new Model.Finder<Long, Question>(Question.class).ref(qsId);

        //sprawdz czy formularz zostal juz zaakceptowany
        if(question.formularz.accepted){
            return status(403, "Formularz został już zaakceptowany");
        }

        //usuniecie wszystkich odpowiedzi zwiazanych z pytaniem
        for (Answer answer : question.answers) {
            answer.delete();
        }

        Long fmId = question.formularz.id;
        question.delete();
        return redirect(routes.FormsController.view(fmId, "questions"));
    }

    //dodawanie pytania do formularza
    public Result addQuestion(Long fmId){
        //jesli formularz zatwierdzony, zablokuj
        Formularz formularz = Ebean.find(Formularz.class, fmId);
        if(formularz.accepted) {
            return status(403, "Formularz został już zaakceptowany");
        }

        Form<Question> questionForm = Form.form(Question.class);
        String httpMethod = request().method();

        //jeśli metoda POST, odebrano formularz
        if(httpMethod.equals("POST")){
            //odczytanie danych z odebranego formularza
            questionForm = questionForm.bindFromRequest();

            //walidacja danych
            if(!questionForm.hasErrors()){
                Question question = questionForm.get();

                //dodanie pytania do formularza
                question.formularz = formularz;
                question.save();

                //zapisanie odpowiedzi do pytania, jesli typ pytania to checkbox lub radio
                if(question.questionType == 2 || question.questionType == 3){
                    //dane z formularza
                    Map<String, String[]> requestData = request().body().asFormUrlEncoded();
                    Answer answer = null;
                   //dodano counter liczby odpowiedzi
                    for(int i = 1; i < 11; i++){
                        String pytanie = requestData.get("ans"+i)[0];
                        //zapisz tylko wypełnione pola do bazy
                        if(!pytanie.isEmpty()) {
                            answer = new Answer();
                            answer.answer = pytanie;
                            answer.question = question;
                            answer.save();
                        }
                    }
                }

                return redirect(routes.FormsController.view(formularz.id, "questions"));
            }

        }

        //wyświetlenie formularza
        return ok(formularzAddQuestionForm.render(questionForm, "Dodaj pytanie"));
    }

    public Result view(Long id, String tab){
        Formularz formularz = Ebean.find(Formularz.class, id);

        //odwrocenie kolejnosci pytan, najnowsze na dole
        Collections.reverse(formularz.questions);

        return ok(formview.render(formularz, tab));
    }

    public Result index() {
        PathProperties pathProperties = PathProperties.parse("(id, name, deadline)");
        List<Formularz> allForms = Ebean.find(Formularz.class).apply(pathProperties).orderBy("id").findList();
        return ok(indexforms.render(allForms));
    }

    public Result create(){
        Form<Formularz> formularzForm = Form.form(Formularz.class);
        String httpMethod = request().method();

        //jeśli metoda POST, odebrano formularz
        if(httpMethod.equals("POST")){
            //odczytanie danych z odebranego formularza
            formularzForm = formularzForm.bindFromRequest();

            //walidacja danych
            if(!formularzForm.hasErrors()){
                //zapisanie do bazy
                Formularz formularz = formularzForm.get();
                formularz.accepted = false;
                formularz.save();
                return redirect(routes.FormsController.index());
            }

        }

        //wyświetlenie formularza
        return ok(formularzform.render(formularzForm, "Dodaj formularz"));
    }

    public Result update(Long id){
        Formularz formularz = new Model.Finder<Long, Formularz>(Formularz.class).byId(id);
        Form<Formularz> formularzForm = Form.form(Formularz.class);
        String httpMethod = request().method();

        //jeśli metoda POST, odebrano formularz
        if(httpMethod.equals("POST")){
            //odczytanie danych z odebranego formularza
            formularzForm = formularzForm.bindFromRequest();

            //walidacja danych
            if(!formularzForm.hasErrors()){
                //zapisanie do bazy
                formularz = formularzForm.get();
                formularz.id = id;
                formularz.update();
                return redirect(routes.FormsController.index());
            }

        }else{
            formularzForm = formularzForm.fill(formularz);
        }

        //wyświetlenie formularza
        return ok(formularzform.render(formularzForm, "Edytuj formularz"));
    }

    public Result delete(Long id){
        Formularz formularz = new Model.Finder<Long, Formularz>(Formularz.class).ref(id);

        //usuniecie pytań zwiazanych z formularzem
        for(Question question: formularz.questions) {
            //usuniecie wszystkich odpowiedzi zwiazanych z pytaniem
            for (Answer answer : question.answers) {
                answer.delete();
            }
            //usuniecie pytania
            question.delete();
        }

        //usunięcie formularza
        formularz.delete();

        //flash który zostanie wyświetlony po przekierowaniu na stronie index
        flash("forms-alert", "Formularz został usunięty");
        return redirect(routes.FormsController.index());
    }

}