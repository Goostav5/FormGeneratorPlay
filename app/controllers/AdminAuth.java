package controllers;

        import play.*;
        import play.mvc.*;
        import play.mvc.Http.*;

        import models.*;

/**
 * Created by Marcin on 2015-12-15.
 * Klasa służy do autoryzacji akcji w innych kontrolerach
 * Ta konkretna sprawdza czy uzytkownik jest adminem i zwraca login lub blokuje dostep
 */
public class AdminAuth extends Security.Authenticator{

    //sprawdzenie czy zalogowany jest admin, jesli zwroci null, dostep jest blokowany
    @Override
    public String getUsername(Context ctx) {
        String adminFlag = ctx.session().get("admin");
        if(adminFlag != null && adminFlag.equals("true")){
            return ctx.session().get("login");
        }else{
            return null;
        }
    }

}
