package controllers;


import com.avaje.ebean.Ebean;
import com.fasterxml.jackson.databind.JsonNode;
import models.*;
import org.apache.commons.collections4.ListUtils;
import play.mvc.Controller;
import com.avaje.ebean.Model;
import play.data.Form;
import play.mvc.*;
import views.html.*;

import java.util.*;

import play.libs.Json;


public class SubmintController extends Controller {

    //akcja wyswietlajaca odpowiedzi podanego uzytkownika (tylko dla admina)
    @Security.Authenticated(AdminAuth.class)
    public Result showUserAnswers(Long fmId, Long usId){
        Submint submint = Ebean.find(Submint.class).where().eq("fm_usr_id", usId).eq("fm_frm_id", fmId).findUnique();

        //odwrocenie kolejnosci pytan, najnowsze na dole
        Collections.reverse(submint.formularz.questions);

        if(submint != null && submint.completed) {
            return ok(formUserAnswers.render(submint));
        }else{
            return status(404, "Ten użytkownik nie wypełnił jeszcze formularza");
        }
    }

    //akcja wyswietlajaca odpowiedzi zalogowanego uzytkownika na pytania
    @Security.Authenticated(UserAuth.class)
    public Result showAnswers(Long id){
        Submint submint = Ebean.find(Submint.class).where().eq("fm_usr_id", new Long(session("userid"))).eq("fm_frm_id", id).findUnique();

        if(submint != null) {
            //stworz liste pytan ktore maja juz odpowiedz
            List<Question> answered = new ArrayList<Question>();
            for (Answer a : submint.answers) {
                answered.add(a.question);
            }
            //usuniecie duplikatow z listy
            LinkedHashSet<Question> lhs = new LinkedHashSet<Question>();
            lhs.addAll(answered);
            answered.clear();
            answered.addAll(lhs);

            return ok(userAnswers.render(submint, answered));
        }else{
            Formularz formularz = Ebean.find(Formularz.class, id);
            return ok(userNoAnswers.render(formularz.name));
        }
    }

    //akcja wyswietlajaca formularz
    @Security.Authenticated(UserAuth.class)
    public Result preprareToFill(Long id){
        Formularz form = Ebean.find(Formularz.class, id);
        User user = Ebean.find(User.class).where().eq("fm_usr_id", new Long(session("userid"))).findUnique();

        //sprawdzenie czy formularz i użytkownik należą do tej samej sekcji
        Boolean match = false;
        for(Section fSection: form.sections){
            for(Section uSection: user.sections){
                if(fSection.id == uSection.id){
                    match = true;
                }
            }
        }
        if(!match){
            return status(403, "Unauthorized");
        }


        //wyrzucenie pytan ktore maja juz odpowiedz
        Submint submint = Ebean.find(Submint.class).where().eq("fm_usr_id", new Long(session("userid"))).eq("fm_frm_id", id).findUnique();
        if(submint != null){
            List<Question> answered = new ArrayList<Question>();
            for(Answer a: submint.answers){
                answered.add(a.question);
            }
            form.questions = ListUtils.subtract(form.questions, answered);
        }

        //odwrocenie kolejnosci pytan, najnowsze na dole
        Collections.reverse(form.questions);
        return ok(userresponsequestion.render(form));
    }

    //akcja zapisująca odpowiedz
    @Security.Authenticated(UserAuth.class)
    public Result saveResponses(){
        Map<String, String[]> requestData = request().body().asFormUrlEncoded();
        String id = requestData.get("questionId")[0];
        String type = requestData.get("questionType")[0];
        String ans = requestData.get("ans")[0];

        if(id == null || type == null || ans == null) {
            return badRequest("brak danych");
        } else {
            Short typeShort = new Short(type);
            Long idLong = new Long(id);
            Long userId = new Long(session("userid"));

            Question question =  Ebean.find(Question.class, idLong);
            Formularz formularz = Ebean.find(Formularz.class, question.formularz.id);
            User user = Ebean.find(User.class, userId);

            //sprawdzenie czy formularz i użytkownik należą do tej samej sekcji
            Boolean match = false;
            for(Section fSection: formularz.sections){
                for(Section uSection: user.sections){
                    if(fSection.id == uSection.id){
                        match = true;
                    }
                }
            }
            if(!match){
                return status(403, "Unauthorized");
            }

            //sprawdzenie czy istnieje już rekord submit dla tego uzytkownika i formularza, ja nie ma to stworz
            Submint submint = Ebean.find(Submint.class).where().eq("fm_usr_id", userId).eq("fm_frm_id", question.formularz.id).findUnique();
            if(submint == null){
                submint = new Submint();
                submint.completed = false;
                submint.userAnswer = user;
                submint.formularz = formularz;
                submint.save();
            }

            if(submint.isCompleted()){
                return ok("completed");
            }

            //sprawdz czy odpowiedz na pytanie juz istnieje
            for(Answer answer: submint.answers){
                if(answer.question.id == question.id){
                    return ok("blad: odpowiedz już istnieje");
                }
            }

            Answer answer;

            switch(typeShort) {
                //text i data
                case 1:
                case 4:
                    //tworzenie nowego rekordu odpowiedzi i zapisanie relacji
                    answer = new Answer();
                    answer.answer = ans;
                    answer.question = question;
                    answer.save();
                    submint.answers.add(answer);
                    submint.update();
                    break;
                //checkbox
                case 2:
                    String[] ansArr = ans.split(",");
                    //zapisanie relacji do wszystkich zaznaczonych odpowiedzi
                    for(String s: ansArr){
                        answer = Ebean.find(Answer.class).where().eq("fm_ans_id", new Long(s)).findUnique();
                        submint.answers.add(answer);
                        submint.update();
                    }
                    break;
                //radio
                case 3:
                    //zapisanie relacji do zaznaczonej odpowiedzi
                    answer = Ebean.find(Answer.class).where().eq("fm_ans_id", new Long(ans)).findUnique();
                    submint.answers.add(answer);
                    submint.update();
                    break;
            }

            if(submint.isCompleted()){
                return ok("completed");
            }

            return ok("success");
        }
    }
}
