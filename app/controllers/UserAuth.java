package controllers;

import play.*;
import play.mvc.*;
import play.mvc.Http.*;

import models.*;

/**
 * Created by Marcin on 2015-12-16.
 * Klasa służy do autoryzacji akcji w innych kontrolerach
 * Ta konkretna sprawdza czy uzytkownik jest zwyklym uzytkownikiem i zwraca login lub blokuje dostep
 */
public class UserAuth extends Security.Authenticator{

    //sprawdzenie czy zalogowany, jesli zwroci null, dostep jest blokowany
    @Override
    public String getUsername(Context ctx) {
        return ctx.session().get("login");
    }

}
