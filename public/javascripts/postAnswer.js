$(document).ready(function(){
    $(".send-answer").click(function(){
        var questionId = $(this).data("question");
        var questionType = $(this).data("type");

        var buttonClicked = $(this);

        //zmienna zawierajaca komunikat bledu walidacji
        var validationError = null;

        //pobranie odpowiedzi na pytanie do zmiennej ans
        switch(questionType) {
            case 1:
                var ans = $('#'+questionId).val();
                if(ans == ''){
                    validationError = 'Musisz wypełnić pole odpowiedzi';
                }else{
                    $('#'+questionId).prop("disabled", true);
                }
                break;
            case 2:
                var ans = '';
                $('#'+questionId+' > input').each(function () {
                    if($(this).prop("checked")){
                        ans += $(this).val()+',';
                    }
                });
                if(ans == ''){
                    validationError = 'Musisz zaznaczyć conajmniej jedną odpowiedź';
                }else{
                    $('#'+questionId+' > input').each(function () {
                        $(this).prop("disabled", true);
                    });
                }
                break;
            case 3:
                var ans = $("input[name='"+questionId+"']:checked").val();
                if(!ans){
                    validationError = 'Musisz zaznaczyć conajmniej jedną odpowiedź';
                }else{
                    $("input[name='"+questionId+"']").each(function(){
                        $(this).prop("disabled", true);
                    })
                }
                break;
            case 4:
                var ans = $('#'+questionId).val();
                if(ans == ''){
                    validationError = 'Musisz wybrać datę';
                }else{
                    $('#'+questionId).prop("disabled", true);
                }
                break;
        }

        if(validationError == null){
            buttonClicked.prop("disabled", true);

            var data = {
                questionId: questionId,
                questionType: questionType,
                ans: ans,
            };

            $.post("/user/saveResponses",
                data,
                function(data, status){
                    if(data == 'completed'){
                        window.location.href = "/user/forms";
                    }
                }
            );
        }else{
            alert(validationError);
        }
    });
})