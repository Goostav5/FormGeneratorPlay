$(document).ready(function(){
        //funkcja obsługująca kliknięcie w przyckiski akcji do dodawania obiektów
        $(document).on('click', '.add-action', function(e){
            e.preventDefault();
            var url = $(this).attr("href");
            //dynamiczne stworzenie i wysłanie ukrytego formularza, żeby była metoda POST
            var form = $('<form action="' + url + '" method="post"></form>');
            $('body').append(form);
            form.submit();
        });
})