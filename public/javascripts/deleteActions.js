$(document).ready(function(){
        //funkcja obsługująca kliknięcie w przyckiski akcji do usuwania obiektow
        $(document).on('click', '.post-action', function(e){
            e.preventDefault();
            if(confirm("Wybrany obiekt zostanie usunięty. Kontynuować?")){
                var url = $(this).attr("href");
                //dynamiczne stworzenie i wysłanie ukrytego formularza, żeby była metoda POST
                var form = $('<form action="' + url + '" method="post"></form>');
                $('body').append(form);
                form.submit();
            }
        });
})