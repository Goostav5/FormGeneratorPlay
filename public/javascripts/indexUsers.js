$(document).ready(function(){

    //funkcja obsługująca kliknięcie w przyckiski akcji
    $(document).on('click', '.post-action', function(e){
        e.preventDefault();
        if(confirm("Wybrany użytkownik zostanie usunięty. Kontynuować?")){
            var url = $(this).attr("href");
            //dynamiczne stworzenie i wysłanie ukrytego formularza, żeby była metoda POST
            var form = $('<form action="' + url + '" method="post"></form>');
            $('body').append(form);
            form.submit();
        }
    });

    //pobranie użytkowników ajaxem i wrzucenie do tabelki
	$.get("/get-users", function(data, status){
	    //ukrycie loadingGifa
	    $("#loadingGif").hide();

	    $("#users").append(function(){
            var tbody = "";
             data = jQuery.parseJSON(data);

            $.each(data, function(i, item) {
                tbody += "<tr>";
                    tbody += "<td>" + data[i].name + "</td>";
                    tbody += "<td>" + data[i].surname + "</td>";
                    tbody += "<td>" + data[i].login + "</td>";
                    tbody += "<td><a href='mailto:" + data[i].email + "'>" + data[i].email + "</a></td>";
                    tbody += "<td>";
                    if(data[i].admin){
                        tbody += "<span class='glyphicon glyphicon-ok'></span>";
                    }else{
                        tbody += "<span class='glyphicon glyphicon-remove'></span>";
                    }
                    tbody += "</td>";
                    var deleteLink = "<a title='Usun' class='post-action' href='/users/delete/" + data[i].id + "'><span class='glyphicon glyphicon-trash'></span></a>";
                    var updateLink = "<a title='Edytuj' href='/users/update/" + data[i].id + "'><span class='glyphicon glyphicon-pencil'></span></a>";
                    var viewLink = "<a title='Szczegóły' href='/users/view/" + data[i].id + "'><span class='glyphicon glyphicon-eye-open'></span></a>";
                    tbody += "<td>" + viewLink + "     " + updateLink + "    " + deleteLink + "</td>";
                tbody += "</tr>";
            })

            return tbody;
        })
    });
});