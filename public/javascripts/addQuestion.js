$(document).ready(function(){
    answersDiv();

    $('#questionType').change(function() {
        answersDiv();
    });

    //walidacja odpowiedzi
    $( "#questionForm" ).submit(function( event ) {
        //policz ilosc odpowiedzi
        var count = 0;
        for(var i = 1; i < 11; i++){
            //sprawdz czy zawiera znaki
            if($('#ans'+i).val()){
                    count++;
            }
        }

        //jesli typ pytania to checkbox lub radio, a jest mnie niż 2 odpowiedzi, blokuj
        var questionType = $('#questionType').val();
        if( (questionType == 2 || questionType == 3) && count < 2){
            $('#error').html("Za mało odpowiedzi!");
            event.preventDefault();
        }
    });
})

//wyswietla lub chowa okienko z odpwiedziami w zaleznosci od typu pytania
function answersDiv() {
    var questionType = $('#questionType').val();
    if(questionType == 1 || questionType == 4){
        countAnswers = 0;
        $('#answers-div').hide();
    }
    else{
        $('#answers-div').show();
    }
}