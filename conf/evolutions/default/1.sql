# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table fm_answers (
  fm_ans_id                 bigserial not null,
  fm_ans_answer             varchar(255),
  fm_que_id                 bigint,
  constraint pk_fm_answers primary key (fm_ans_id))
;

create table fm_forms (
  fm_frm_id                 bigserial not null,
  fm_frm_name               varchar(255),
  fm_frm_deadline           timestamp,
  fm_frm_accepted           boolean,
  constraint pk_fm_forms primary key (fm_frm_id))
;

create table fm_questions (
  fm_que_id                 bigserial not null,
  fm_que_question           varchar(255),
  fm_que_type               smallint,
  fm_frm_id                 bigint,
  constraint pk_fm_questions primary key (fm_que_id))
;

create table fm_sections (
  fm_sec_id                 bigserial not null,
  fm_sec_name               varchar(255),
  constraint pk_fm_sections primary key (fm_sec_id))
;

create table fm_submits (
  fm_fms_id                 bigserial not null,
  fm_fms_completed          boolean,
  fm_usr_id                 bigint,
  fm_frm_id                 bigint,
  constraint pk_fm_submits primary key (fm_fms_id))
;

create table fm_users (
  fm_usr_id                 bigserial not null,
  fm_usr_name               varchar(255),
  fm_usr_surname            varchar(255),
  fm_usr_email              varchar(255),
  fm_usr_login              varchar(255),
  fm_usr_password           varchar(255),
  fm_usr_admin              boolean,
  constraint uq_fm_users_fm_usr_login unique (fm_usr_login),
  constraint pk_fm_users primary key (fm_usr_id))
;


create table fm_submints_answers (
  fm_ans_id                      bigint not null,
  fm_fms_id                      bigint not null,
  constraint pk_fm_submints_answers primary key (fm_ans_id, fm_fms_id))
;

create table fm_access (
  fm_frm_id                      bigint not null,
  fm_sec_id                      bigint not null,
  constraint pk_fm_access primary key (fm_frm_id, fm_sec_id))
;

create table fm_assignment (
  fm_sec_id                      bigint not null,
  fm_usr_id                      bigint not null,
  constraint pk_fm_assignment primary key (fm_sec_id, fm_usr_id))
;
alter table fm_answers add constraint fk_fm_answers_question_1 foreign key (fm_que_id) references fm_questions (fm_que_id);
create index ix_fm_answers_question_1 on fm_answers (fm_que_id);
alter table fm_questions add constraint fk_fm_questions_formularz_2 foreign key (fm_frm_id) references fm_forms (fm_frm_id);
create index ix_fm_questions_formularz_2 on fm_questions (fm_frm_id);
alter table fm_submits add constraint fk_fm_submits_userAnswer_3 foreign key (fm_usr_id) references fm_users (fm_usr_id);
create index ix_fm_submits_userAnswer_3 on fm_submits (fm_usr_id);
alter table fm_submits add constraint fk_fm_submits_formularz_4 foreign key (fm_frm_id) references fm_forms (fm_frm_id);
create index ix_fm_submits_formularz_4 on fm_submits (fm_frm_id);



alter table fm_submints_answers add constraint fk_fm_submints_answers_fm_ans_01 foreign key (fm_ans_id) references fm_answers (fm_ans_id);

alter table fm_submints_answers add constraint fk_fm_submints_answers_fm_sub_02 foreign key (fm_fms_id) references fm_submits (fm_fms_id);

alter table fm_access add constraint fk_fm_access_fm_forms_01 foreign key (fm_frm_id) references fm_forms (fm_frm_id);

alter table fm_access add constraint fk_fm_access_fm_sections_02 foreign key (fm_sec_id) references fm_sections (fm_sec_id);

alter table fm_assignment add constraint fk_fm_assignment_fm_sections_01 foreign key (fm_sec_id) references fm_sections (fm_sec_id);

alter table fm_assignment add constraint fk_fm_assignment_fm_users_02 foreign key (fm_usr_id) references fm_users (fm_usr_id);

# --- !Downs

drop table if exists fm_answers cascade;

drop table if exists fm_submints_answers cascade;

drop table if exists fm_forms cascade;

drop table if exists fm_access cascade;

drop table if exists fm_questions cascade;

drop table if exists fm_sections cascade;

drop table if exists fm_assignment cascade;

drop table if exists fm_submits cascade;

drop table if exists fm_users cascade;

